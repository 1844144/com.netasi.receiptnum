<?php
// + create tables
//  display additional info in contribution view
//  assign receipt number as search task !!
//  edit receipt template
//  search by receipt number ability ??

// explanation
// Individual receipts will be assigned with your current plugin but their will need to be some kind of automation to assign all the other individual receipts and year-end receipt at the end of the year
// We don't want to have to do that all manually.
// Tax numbers assigned before the end of the year and normally printed before the end of the year also.
// This is because someone requests a receipt so it is manually generated
// If it is generated before the end of the year it WILL NOT be printed again at the end of the year.
// There is one other necessary element
// Sometimes people lose their receipts and ask for another copy.
// When that happens Canada law requires us to issue a NEW number but to list the OLD number on the receipt as well with a message like this
// This receipt replace receipt number OLD
// I think you have all the elements now ☺
// But there may be more surprises.
// ☺
// BUT ...
// If someone made a donation on September 15 and you generated and printed a receipt then they made a donation on October 15 but no receipt was generated.. At the end of the year you'll need to generate an individual receipt for the oct. 15 donation.

// Separate receipt
// 1. Add task for contiribution "Print TAX receipt" - mark as printed and generate one time screen with html - next time it will be duplicatie. Analyze if we will process FIRST TIME this year printing, group by user 
// 2. For contribution search - add ability to find not printed, and not assigned   

// for all user add ability to print yearly receipt (by template)


require_once 'receiptnum.civix.php';

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function receiptnum_civicrm_config(&$config) {
  _receiptnum_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function receiptnum_civicrm_xmlMenu(&$files) {
  _receiptnum_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function receiptnum_civicrm_install() {
  _receiptnum_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function receiptnum_civicrm_uninstall() {
  _receiptnum_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function receiptnum_civicrm_enable() {
  _receiptnum_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function receiptnum_civicrm_disable() {
  _receiptnum_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function receiptnum_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _receiptnum_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function receiptnum_civicrm_managed(&$entities) {
  _receiptnum_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function receiptnum_civicrm_caseTypes(&$caseTypes) {
  _receiptnum_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function receiptnum_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _receiptnum_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

function receiptnum_civicrm_entityTypes(&$entityTypes) {
 $entityTypes[] = array(
   'name' => 'ReceiptNumber',
    'class' => 'CRM_Receiptnum_DAO_ReceiptNumber',
    'table' => 'civicrm_receiptnumber',
 );
}

function receiptnum_civicrm_buildForm($formName, &$form){

  // if ($formName == 'CRM_Export_Form_Map') {
  //   error_log(print_r($form,1), 3, '/tmp/myerr1');
  // }

  // adding search file 
  if ($formName == 'CRM_Contribute_Form_Search') {
     $form->add('text', 'rcpt_num', ts('Tax Receipt Number'));

     $templatePath = realpath(dirname(__FILE__));
    // Add the field element in the form
    CRM_Core_Region::instance('page-body')->add(array(
      'template' => "{$templatePath}/ff.tpl"
     ));
  }

  if (( $formName == 'CRM_Contribute_Form_Search' ) || ($formName == 'CRM_Contact_Form_Search_Basic')) {
    CRM_Core_Resources::singleton()->addScriptFile('com.netasi.receiptnum', 'innewtab.js');
  }



}
function receiptnum_civicrm_preProcess($formName, &$form){

  if ($formName == 'CRM_Contribute_Form_ContributionView') {
    $id = $form->get('id');
    $values = $ids = array();
    $params = array('id' => $id);

    CRM_Contribute_BAO_Contribution::getValues($params, $values, $ids);

    $cid = $values['id'];

    $sql = " SELECT * from civicrm_receiptnumber rn where rn.contribution_id=$cid order by number desc";
    $dao = CRM_Core_DAO::executeQuery($sql);
    if ( $dao->fetch() ) {
        $number = $dao->number;
        // add past numbers
        $past = CRM_Contribute_Form_Task_Printtax::get_previous_numbers($number);
        if (!empty($past)) {
          $past = join(',', $past);
          $number = $number." (past numbers $past)";
        }
    }
    else {
      $number = ' -- Not assigned --';
    }

    $form->assign('receiptnum', array(  'number'=> $number ));
   $templatePath = realpath(dirname(__FILE__)."/templates");
    CRM_Core_Region::instance('page-body')->add(array(
      'template' => "{$templatePath}/field.tpl"
    ));
   
  }
  
}


// function receiptnum_civicrm_postProcess($formName, &$form){

//   if ($formName == 'CRM_Contribute_Form_Search') {
//     // evar_dump($form);
//   }
//   if ($formName == 'CRM_Contribute_Form_Task_PDFLetter') {
//     // $data = &$form->controller->container();
//     // $the_name = $form->getAttribute('name');
//     // $data['values'][$the_name]['html_message'] = "hepp";



  
//   }


// }

function receiptnum_civicrm_validateForm($formName, &$fields, &$files, &$form, &$errors){

  if ($formName == 'CRM_Contribute_Form_Task_PDFLetter') {
    $GLOBALS['receiptnum_contriution_ids'] = $form->getVar('_contributionIds');
  }


}


function receiptnum_civicrm_searchTasks($objectType, &$tasks )
{
    if ($objectType=='contribution')
    {
        // $tasks[] = array(
        //     'title'  => ts( 'Assign Tax Receipt Number'),
        //     'class'  => 'CRM_Contribute_Form_Task_Receiptnum',
        //     'result' => false );

        $tasks[] = array(
            'title'  => ts( 'Print Tax Receipts'),
            'class'  => 'CRM_Contribute_Form_Task_Printtax',
            'result' => false );
    }
    if ($objectType=='contact')
    {
        $tasks[] = array(
            'title'  => ts( 'Print Year-end Tax Receipts'),
            'class'  => 'CRM_Contribute_Form_Task_Yearly',
            'result' => false );
    }
}

function receiptnum_civicrm_tokens(&$tokens) {

  // Add Manager Tokens
  $tokens['Tax Receipt'] = [
    "tax_receipt.number" => "Tax Receipt Number",
    "tax_receipt.yearly_total" => "Yearly Total",
    "tax_receipt.issued" => "Receipt issued date",
    "tax_receipt.date_complete" => "Today's date",
    "tax_receipt.spouse_if_no_contribution" => "Spouse if no contribution",
  ];

}

function receiptnum_civicrm_queryObjects(&$queryObjects, $type) {
  // evar_dump($queryObjects);
  // evar_dump($type);
  if ($type == 'Contact') {
    $queryObjects[] = new CRM_Receiptnum_BAO_Query();
  }
  // elseif ($type == 'Report') {
  //   $queryObjects[] = new CRM_HRJob_BAO_ReportHook();
  // }
}


function date_civi_to_php($in){
  $convertion_rules = array(
                  'b' =>  'D',
                'B' =>  'F',
                'd' =>  'd',
                'e' =>  'j', 
                'E' =>  'j', 
                'f' =>  'S', 
                'H' =>  'H', 
                'I' =>  'h', 
                'k' =>  'G', 
                'l' =>  'g', 
                'm' =>  'm', 
                'M' =>  'i', 
                'p' =>  'a',
                'P' =>  'A', 
                'Y' =>  'Y',
                  );
                $percent = False;
                $out = "";
                for ($i=0; $i < strlen($in); $i++) { 
                  $char = $in[$i];
                  if ($percent) {
                    $out .= $convertion_rules[$char];
                    $percent = False;
                    continue;
                  }
                  if ($char == '%') {
                    $percent = True;
                  }
                  else $out .= $char;

                }
        return $out;

}

function receiptnum_civicrm_tokenValues(&$values, $cids, $job = null, $tokens = array(), $context = null) {
  // Date tokens

    // error_log('in receiptnum');
    // evar_dump($cids);
    // evar_dump($tokens);
    // evar_dump($context);
    // evar_dump($values);
    // evar_dump($job);

    // evar_dump($GLOBALS['receiptnum_contriution_ids']);

    $GLOBALS['receiptnum_contriution_ids'];


    foreach ($cids as $cid) {
      // evar_dump($cid);

        // account manager tokens
        if (array_key_exists('tax_receipt', $tokens)) {
            

                $result = civicrm_api3('ReceiptNumber', 'get', array(
                'sequential' => 1,
                'contribution_id' => $cid,
              ));



                // $values[$cid]['account_manager.firstname'] = $result['first_name'];
                // $values[$cid]['account_manager.lastname'] = $result['last_name'];
                $values[$cid]['tax_receipt.number'] = $result['values'][0]['number'];
                
                // check for prefix suffix
                // $values[$cid]['account_manager.suffix'] = $result['individual_suffix'];
                // $values[$cid]['account_manager.prefix'] = $result['individual_prefix'];

                 $result = civicrm_api3('Setting', 'get', array(
                  'sequential' => 1,
                  'dateformatFull' => "",
                ));

                $date_format = $result['values'][0]['dateformatFull'];
                $date_format = date_civi_to_php($date_format);
                $values[$cid]['tax_receipt.date_complete'] = date($date_format);

      }

    
    }
}


function receiptnum_civicrm_navigationMenu(&$params) {
 
  // Check that our item doesn't already exist
  $menu_item_search = array('url' => 'civicrm/receiptnum/config');
  $menu_items = array();
  CRM_Core_BAO_Navigation::retrieve($menu_item_search, $menu_items);
 
  if ( ! empty($menu_items) ) { 
    return;
  }
 
  $navId = CRM_Core_DAO::singleValueQuery("SELECT max(id) FROM civicrm_navigation");
  if (is_integer($navId)) {
    $navId++;
  }
  // Find the Report menu
  $reportID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_Navigation', 'Contributions', 'id', 'name');
      $params[$reportID]['child'][$navId] = array (
        'attributes' => array (
          'label' => ts('Tax Receipt Options',array('domain' => 'com.netasi')),
          'name' => 'Tax Receipt Options',
          'url' => 'civicrm/receiptnum/config',
          'permission' => 'access CiviContribute',
          'operator' => 'OR',
          'separator' => 2,
          'parentID' => $reportID,
          'navID' => $navId,
          'active' => 1
    )   
  );  
}

// catching ???
// function receiptnum_civicrm_queryObjects(&$queryObjects, $type){
//   error_log('Catched queryObject');
//   error_log(print_r($queryObjects,1));
//   error_log(print_r($type,1));
// }

// function receiptnum_civicrm_buildForm($formName, &$form) {
//   // error_log(print_r($formName,1));
//   // error_log(print_r($form,1));
// }

// function receiptnum_civicrm_fieldOptions($entity, $field, &$options, $params){
//   // error_log('Catched fieldoptions');
//   // error_log(print_r($entity,1));
//   error_log(print_r($field,1));
//   // error_log(print_r($options,1));
//   // error_log(print_r($params,1));

// }

function receiptnum_civicrm_export( $exportTempTable, &$headerRows, &$sqlColumns, $exportMode )
{

  if ($exportMode==2) {
     
    // Add the four columns for financial coding
    $sql = "ALTER TABLE ".$exportTempTable." ";
    $sql .= "ADD COLUMN receipt_number int";
     
    CRM_Core_DAO::singleValueQuery($sql);
     
    // Populate them from the source table
    $sql = "UPDATE ".$exportTempTable." a ";
    $sql .= "JOIN (select contribution_id, max(number) as number from civicrm_receiptnumber group by contribution_id ) b ON a.contribution_id = b.contribution_id ";
    $sql .= "SET receipt_number = b.number ";
     
    CRM_Core_DAO::singleValueQuery($sql);
     
    // Ensure everything is added to the $headerRows and $sqlColumns
    $headerRows[] = "Tax Receipt Number";
   
    $sqlColumns['receipt_number'] = 'number int';
  }
  
}