
CREATE TABLE IF  NOT EXISTS civicrm_receiptnumber(

     id int unsigned NOT NULL AUTO_INCREMENT  COMMENT 'ID',
     contribution_id int unsigned NOT NULL   COMMENT 'FK to Contribution',
     number int NOT NULL  COMMENT 'Receipt Number',
     printed tinyint(1) default 0,
     oldnumber int default 0 COMMENT 'Old Receipt Number',
     yearly tinyint(1) default 0,
     issued date ,

    PRIMARY KEY ( id )


	, INDEX FKEY_contribution_id ( contribution_id ) ,
     FOREIGN KEY (contribution_id) REFERENCES civicrm_contribution(id)

) 

