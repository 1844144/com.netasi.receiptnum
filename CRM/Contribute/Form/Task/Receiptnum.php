<?php


class CRM_Contribute_Form_Task_Receiptnum extends CRM_Contribute_Form_Task {
    
    public function check_config(){
        $tmpl_yearly = get_option('receiptnum_template_yearly', NULL);
        $tmpl_individual = get_option('receiptnum_template_usual', NULL);
        if ($tmpl_yearly == NULL || $tmpl_individual == NULL) {
          $session = CRM_Core_Session::singleton();
           $session->setStatus(ts('You have to set templates for tax receipts'), ts('Tax receipt options'),  'warning');
          CRM_Utils_System::redirect(
                    CRM_Utils_System::url(
                      'civicrm/receiptnum/config'
                    )
          );
        }
    }

    public function assign_receipt_number($ids_list,$lost = False, $yearly_receipt= False ){
        // given contribution ids_list - assign tax numbers and return (num,name,date,amount) 
        // array

        self::check_config();
        $tx = new CRM_Core_Transaction();

        $all = array();
        if (empty($ids_list)) return $all;

        $sql = "select max(number) as maxn from civicrm_receiptnumber ";
        $dao = CRM_Core_DAO::executeQuery($sql);

        $max = 100000;
        $res = $dao->fetch();
        if ( $dao->maxn) {
            $max = $dao->maxn;
        } 

        // get all contribution with financial type Dedcutible
        // sorted by user and date and without!!! assigned  tax receipt number
        // 
        // process it by assigning numbers and print results 
        $ids_list  = join(',', $ids_list);

        if ($lost) $condition = '';
        else $condition = "and rn.id IS NULL";

        $sql = "SELECT  con.display_name, cn.receive_date, cn.total_amount, con.id as contact_id, cn.id as contribution_id, rn.id as rn_id, rn.number, rn.yearly from civicrm_contribution cn 
            left outer join  civicrm_receiptnumber rn on rn.contribution_id=cn.id
            left join civicrm_financial_type ft on ft.id=cn.financial_type_id
            left join civicrm_contact con on con.id=cn.contact_id
            where cn.id in ({$ids_list}) $condition and ft.is_deductible=1 and cn.contribution_status_id=1 order by cn.contact_id, cn.receive_date, rn.number desc "; 

        $dao = CRM_Core_DAO::executeQuery($sql);
        
        // for first case
        $current_id = -1;
        $current_contribution = -1;
        while ($dao->fetch()) {
            if ($current_contribution == $dao->contribution_id) continue;
            $current_contribution = $dao->contribution_id;

            if ($yearly_receipt) {
                if ($current_id != $dao->contact_id) {
                    $current_id = $dao->contact_id;
                    // and use new receipt num
                    $max = $max + 1;
                }
            }
            else $max = $max + 1;


            $all[] = array (
                'num' => $max,
                'name' => $dao->display_name,
                'date' => date('F j, Y', strtotime(date($dao->receive_date))), 
                'amount' => $dao->total_amount,
            );
            $yearly = $yearly_receipt ? 1:0;
            if ($lost) $sql2 = "Insert into civicrm_receiptnumber (contribution_id, number,oldnumber,issued, yearly) values ({$dao->contribution_id}, {$max}, {$dao->number}, NOW(), {$dao->yearly} ) "; 
            else $sql2 = "Insert into civicrm_receiptnumber (contribution_id, number, yearly, issued) values ({$dao->contribution_id}, {$max}, {$yearly}, now())";
            CRM_Core_DAO::executeQuery($sql2);
        } 
        return $all;
    }

    function buildQuickForm()
    {

        $ids_list = $this->_contributionIds;

        // evar_dump ($ids_list);
        $all = self::assign_receipt_number($ids_list, False, True);

        $this->assign('data',$all);
        CRM_Utils_System::setTitle('Processed items');
    }

	public function postProcess(){
		// error_log($this->_contactIds);
	}

}
