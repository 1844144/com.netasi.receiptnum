<?php


class CRM_Contribute_Form_Task_Printtax extends CRM_Contribute_Form_Task {
    
    public function get_previous_numbers($number){
        $out = array();
        while (True) {
            $sql = "select oldnumber from civicrm_receiptnumber where number = {$number}";
            $dao = CRM_Core_DAO::executeQuery($sql);
            if (! $dao->fetch()) break;
            $oldnumber = $dao->oldnumber;

            if ($oldnumber == 0 ) break;
            $out[] = $oldnumber;
            $number = $oldnumber;
        }
        return $out;
    }
    public function get_receipt_ids($lid){
        $sql = "select contribution_id from civicrm_receiptnumber where number={$lid}";
            $dao = CRM_Core_DAO::executeQuery($sql);
            $this_receipt_ids  = array();
            while($dao->fetch()) $this_receipt_ids[] = $dao->contribution_id;
            return $this_receipt_ids;
    }
    

    function buildQuickForm()
    {
        $ids = $this->_contributionIds;

        // 0 filter contrubution Ids by deductible
        // 1 check for not assigned receipt num
        // 2 check for already printed receipts 

        // step 0 
        $ids_list = join(',', $ids);
        $ids = array();
        $sql = "SELECT  cn.id from civicrm_contribution cn 
            left join civicrm_financial_type ft on ft.id=cn.financial_type_id
            where cn.id in ({$ids_list}) and ft.is_deductible=1  ";
        $dao = CRM_Core_DAO::executeQuery($sql);
        while ( $dao->fetch() ) {
            $ids[] = $dao->id;
        }
        $this->_contributionIds = $ids; 
        $this->setContactIDs();
        $ids_list = join(',', $ids);
        $not_assigned_list = array();       
        $already_printed = array();
        $join_to_yearly = false;

        if (!empty($ids)){
            //step 1
            $year = date("Y",strtotime("-1 year"));
            $sql = " SELECT con.id, cn.display_name, con.total_amount, rn.number, YEAR(con.receive_date) as year, con.contact_id  from civicrm_contribution  con
                left join civicrm_contact cn on cn.id=con.contact_id 
                left outer join civicrm_receiptnumber rn on con.id=rn.contribution_id where con.id in ({$ids_list}) and rn.number IS NULL ";
            
            $dao = CRM_Core_DAO::executeQuery($sql);
            while ( $dao->fetch() ) {
                $not_assigned_list[] = array ( 
                    'id' => $dao->id,
                    'name' => $dao->display_name,
                    'amount' => $dao->total_amount,
                    'year' => $dao->year,
                    'contact_id' => $dao->contact_id,
                );
            }
            

            //step 2
            $sql = " SELECT distinct con.id, cn.display_name, con.total_amount, rn.number, rn.yearly, con.contact_id  from civicrm_receiptnumber rn
                left join civicrm_contribution con on rn.contribution_id=con.id
                left join civicrm_contact cn on cn.id=con.contact_id
                where rn.printed=1 and con.id in ({$ids_list})";
            $dao = CRM_Core_DAO::executeQuery($sql);
            while ( $dao->fetch() ) {
                $already_printed[] = array ( 
                    'id' => $dao->id,
                    'name' => $dao->display_name,
                    'amount' => $dao->total_amount,
                    'number' => $dao->number,
                    'yearly' => $dao->yearly,
                    'contact_id' => $dao->contact_id,
                );
            }

            // And we checking if user want to add receipt to yearly receipt
            $same_number_and_id = true;
            $number = $already_printed[0]['number'];
            $cid = $not_assigned_list[0]['contact_id'];
            foreach ($already_printed as $val) {
                if (!$val['yearly'] || $number!=$val['number'] || $cid!=$val['contact_id']) 
                    $same_number_and_id = false;
            }
            $same_year_and_id = true;
            foreach ($not_assigned_list as $val) {
                if ($cid != $val['contact_id'] || $year!=$val['year']) $same_year_and_id = false;
            }
            if ($same_year_and_id && $same_number_and_id) $join_to_yearly  = true;

        }
        $this->join_to_yearly = $join_to_yearly;

        

        $this->assign('items_count', count ($ids));

        if (!empty($already_printed) or !empty($not_assigned_list)) {

            if (!empty($already_printed)){

                $this->add('select', 'printed', ts('Action '),
                  array(
                    1 => ts('Print duplicate'),
                    0 => ts("Don't print"),
                    2 => ts('Replace and reprint lost receipt'),
                    )
                );
                $this->assign('already_printed', $already_printed);
                $this->already_printed = $already_printed;
            }
            if (!empty($not_assigned_list)) {
                $dropdown_options =  array(
                    1 => ts('Assign number and print receipt'),
                    0 => ts("Don't assign number or print receipt"),
                    );
                if ($join_to_yearly) $dropdown_options[2] = ts("Add contribution(s) to receipt #{$already_printed[0]['number']} and reprint");
                $this->add('select', 'not_assigned', ts('Action '), $dropdown_options   );
                $this->assign('not_assigned_list', $not_assigned_list);
                $this->not_assigned_list = $not_assigned_list;
            }
            
        }
        $buttonsArray = array();
        if (count ($ids)) {
            $buttonsArray[] = array(
              'type' => 'next',
              'name' => ts('Continue'),
              'isDefault' => TRUE,
            ); 
        }
        $buttonsArray[] =  array(
              'type' => 'back',
              'name' => ts('Cancel'),
            );
        $this->addButtons( $buttonsArray);
        CRM_Utils_System::setTitle('Tax Receipt Printing');
    }


    public function postProcess(){

        $to_assign_list = array();
        $to_skip_list = array();
        $yearly_receipts = array();

        $key = 'not_assigned';
        if (array_key_exists($key, $this->_submitValues)) {
            $not_assigned = $this->_submitValues[$key];
            if ($not_assigned==0) {
                // just skip it
                $to_skip_list = array_merge($to_skip_list, $this->not_assigned_list);
            } else if ($not_assigned==1) {
                // process it
                $to_assign_list = $this->not_assigned_list;
            } else if ($not_assigned==2){
                // want to join
                
                // in future skip it
                $to_skip_list = array_merge($to_skip_list, $this->not_assigned_list);
                
                // join these contributions to this yearly receipt
                $number = $this->already_printed[0]['number'];
                foreach ($to_skip_list as $con) {
                    $sql = "Insert into civicrm_receiptnumber (contribution_id, number, issued, yearly, printed) values ({$con['id']}, {$number}, NOW(), 1, 1 ) ";
                    $dao = CRM_Core_DAO::executeQuery($sql);
                }


            }                        
        }
        $key = 'printed';
        if (array_key_exists($key, $this->_submitValues)) {
            $printed = $this->_submitValues[$key];
            if ($this->join_to_yearly) $printed = 2;
            if ($printed==0) {
                //just skip it
                $to_skip_list = array_merge($to_skip_list, $this->already_printed);
            }
            else if ($printed == 2 || $printed == 1){
                // handle lost receipts reprint
                $lost_ids = array();
                foreach ($this->already_printed as $record) {
                    if ($record['yearly']) {
                        $yearly_receipts[] = $record['number'];
                        // remove it from list of ids to print
                        $key  = array_search($record['id'], $this->_contributionIds);
                        if ($key !== false) {
                            unset ($this->_contributionIds[$key]);
                            unset ($this->_contactIds[$key]);
                        }
                    }
                    else $lost_ids[]= $record['id'];
                }


                // now go thru yearly receipt number and get all contribution ids
                $yearly_receipts = array_unique($yearly_receipts);
                // remove previous receipts
                $tmp2  = $yearly_receipts;
                foreach ($tmp2 as $num) {
                    $yearly_receipts = array_diff($yearly_receipts, self::get_previous_numbers($num));
                }
                // make assignation only for Lost, (not for duplicate)
                if ($printed == 2){ 
                    foreach ($yearly_receipts as $lid) {
                        $this_receipt_ids = self::get_receipt_ids($lid);
                        CRM_Contribute_Form_Task_Receiptnum::assign_receipt_number($this_receipt_ids, True, True );
                    }
                    CRM_Contribute_Form_Task_Receiptnum::assign_receipt_number($lost_ids, True );
                }
            }
        }

        parent::postProcess();
        $session = CRM_Core_Session::singleton();
        $session->set('contactIds', $this->_contactIds);
        $session->set('contributionIds', $this->_contributionIds);
        $session->set('to_assign_list', $to_assign_list);
        $session->set('to_skip_list', $to_skip_list);
        $session->set('yearly_receipts', $yearly_receipts);
         $session->replaceUserContext(CRM_Utils_System::url("civicrm/receiptnum/printtax2"));
		
	}

}
