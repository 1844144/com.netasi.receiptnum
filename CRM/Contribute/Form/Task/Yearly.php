<?php


class CRM_Contribute_Form_Task_Yearly extends CRM_Contact_Form_Task {
    
    function buildQuickForm()
    {
        $contactIds = $this->_contactIds;

        // evar_dump($contactIds);

        $out = array();
        $tmpl_yearly = get_option('receiptnum_template_yearly', NULL);
        $tmpl_individual = get_option('receiptnum_template_usual', NULL);


        foreach ($contactIds as $cid) {

            // 1 get all contribution deductible
            // 2 check if it is yearly 
            // 3 print it 
            $year = date("Y",strtotime("-1 year"));

            $sql = "SELECT  cn.id, rn.number, rn.yearly, rn.printed, cn.total_amount from civicrm_contribution cn 
                left join civicrm_contact con on con.id=cn.contact_id
                left outer join civicrm_receiptnumber rn on rn.contribution_id=cn.id
                left join civicrm_financial_type ft on ft.id=cn.financial_type_id
                where con.id={$cid} and year(cn.receive_date)={$year} and ft.is_deductible = 1 and cn.contribution_status_id=1";
            $dao = CRM_Core_DAO::executeQuery($sql);
            $ids = array();
            $has_printed_receipt = false;
            $have_to_reprint_yearly = false;
            $have_to_reprint_individual = false;

            $total_amount = 0;

            while ($dao->fetch()) {
                if ($dao->number == NULL) {
                    $ids[]=$dao->id;
                    $total_amount += $dao->total_amount;
                }
                else {
                    $has_printed_receipt = true;
                    if ($dao->printed == 0){

                        $ids[]=$dao->id;
                        $total_amount += $dao->total_amount;
                        
                        if ($dao->yearly == 1){
                            $have_to_reprint_yearly = true;
                        }
                        else $have_to_reprint_individual = true;
                    }
                }
            }

            if ($have_to_reprint_individual && $have_to_reprint_yearly) {
                // Special Case - ERROR
                $out[] = "<h2> ERROR for cid $cid database has yearly and individual receipts in the Same time";
                continue;
            }

            if ((count ($ids) > 0) && ($total_amount > 0) ){
                if ($has_printed_receipt && !$have_to_reprint_yearly) {
                    // we will print individual receipts
                    if (!$have_to_reprint_individual) {
                        CRM_Contribute_Form_Task_Receiptnum::assign_receipt_number($ids, False, False);
                    }
                    foreach ($ids as $contribution) {
                        $out[] = CRM_Receiptnum_Form_Printtax2::generate_html($contribution, $cid, $tmpl_individual);
                    }
                }
                else {
                    // we will print YEARLY receipts
                    if (!$have_to_reprint_yearly) {
                        CRM_Contribute_Form_Task_Receiptnum::assign_receipt_number($ids, False, True);
                    } 
                        $out[] = CRM_Receiptnum_Form_Printtax2::generate_html($ids[count($ids)-1], $cid, $tmpl_yearly,$ids);
                }

            }
        }

    if (empty($out)) $out[] = "All receipts for all tax deductible contributions have already been printed.";


    $out =  join('',$out);

    $out = '<html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <style>@page { margin: 0.4in 0.3in 0.1in 0.3in; }</style>
          <style type="text/css" src="/wp-content/plugins/civicrm/civicrm/css/print.css">);</style>
    
        </head>
      <body>
        <div id="crm-container"> '  
        . file_get_contents(dirname(__FILE__).'/./Style.tpl') 
        . $out
        . ' </body></html>';
    echo $out;
    CRM_Utils_System::civiExit(1);
        
    // $this->controller->setPrint(1);
    $this->controller->setEmbedded(1);
    $this->assign('html_array', $out);
    }

}
