<?php
/*
*/
/**
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2015
 *
 */
require_once 'CRM/Core/DAO.php';
require_once 'CRM/Utils/Type.php';
class CRM_Receiptnum_DAO_ReceiptNumber extends CRM_Core_DAO
{
  /**
   * static instance to hold the table name
   *
   * @var string
   */
  static $_tableName = 'civicrm_receiptnumber';
  /**
   * static instance to hold the field values
   *
   * @var array
   */
  static $_fields = null;
  /**
   * static instance to hold the keys used in $_fields for each field.
   *
   * @var array
   */
  static $_fieldKeys = null;
  /**
   * static instance to hold the FK relationships
   *
   * @var string
   */
  static $_links = null;
  /**
   * static instance to hold the values that can
   * be imported
   *
   * @var array
   */
  static $_import = null;
  /**
   * static instance to hold the values that can
   * be exported
   *
   * @var array
   */
  static $_export = null;
  /**
   * static value to see if we should log any modifications to
   * this table in the civicrm_log table
   *
   * @var boolean
   */
  static $_log = false;
  /**
   * Unique Address ID
   *
   * @var int unsigned
   */
  public $id;
  /**
   * Variable name/programmatic handle for this batch.
   *
   * @var string
   */
  public $name;
  /**
   * Friendly Name.
   *
   * @var string
   */
  public $title;
  /**
   * Description of this batch set.
   *
   * @var text
   */
  public $description;
  /**
   * FK to Contact ID
   *
   * @var int unsigned
   */
  public $created_id;
  /**
   * When was this item created
   *
   * @var datetime
   */
  public $created_date;
  /**
   * FK to Contact ID
   *
   * @var int unsigned
   */
  public $modified_id;
  /**
   * When was this item created
   *
   * @var datetime
   */
  public $modified_date;
  /**
   * FK to Saved Search ID
   *
   * @var int unsigned
   */
  public $saved_search_id;
  /**
   * fk to Batch Status options in civicrm_option_values
   *
   * @var int unsigned
   */
  public $status_id;
  /**
   * fk to Batch Type options in civicrm_option_values
   *
   * @var int unsigned
   */
  public $type_id;
  /**
   * fk to Batch mode options in civicrm_option_values
   *
   * @var int unsigned
   */
  public $mode_id;
  /**
   * Total amount for this batch.
   *
   * @var float
   */
  public $total;
  /**
   * Number of items in a batch.
   *
   * @var int unsigned
   */
  public $item_count;
  /**
   * fk to Payment Instrument options in civicrm_option_values
   *
   * @var int unsigned
   */
  public $payment_instrument_id;
  /**
   *
   * @var datetime
   */
  public $exported_date;
  /**
   * cache entered data
   *
   * @var longtext
   */
  public $data;
  /**
   * class constructor
   *
   * @return civicrm_batch
   */
  function __construct()
  {
    $this->__table = 'civicrm_receiptnumber';
    parent::__construct();
  }
  /**
   * Returns foreign keys and entity references
   *
   * @return array
   *   [CRM_Core_Reference_Interface]
   */
  static function getReferenceColumns()
  {
    if (!self::$_links) {
      self::$_links = static ::createReferenceColumns(__CLASS__);
      self::$_links[] = new CRM_Core_Reference_Basic(self::getTableName() , 'contribution_id', 'civicrm_contribution', 'id');
    }
    return self::$_links;
  }
  /**
   * Returns all the column names of this table
   *
   * @return array
   */
  static function &fields()
  {
    if (!(self::$_fields)) {
      self::$_fields = array(
        'id' => array(
          'name' => 'id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('ReceiptNumber ID') ,
          'description' => 'Receipt Number ID',
          'required' => true,
        ) ,
        'contribution_id' => array(
          'name' => 'contribution_id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('Contribution ID') ,
          'description' => 'FK to Contribution ID',
          'FKClassName' => 'CRM_Contribution_DAO_Contribution',
        ) ,
        'number' => array(
          'name' => 'number',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('Receipt Number') ,
          'description' => 'Receipt Number of Contribution',
          'html' => array(
            'type' => 'Text',
          ) ,
        ) ,
        'printed' => array(
          'name' => 'printed',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('Printed') ,
          'description' => 'If it was printed',
          'html' => array(
            'type' => 'Text',
          ) ,
        ) ,
        'oldnumber' => array(
          'name' => 'oldnumber',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('Old Receipt Number') ,
          'description' => 'Old Receipt Number',
          'html' => array(
            'type' => 'Text',
          ) ,
        ) ,
        'yearly' => array(
          'name' => 'yearly',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('Printed') ,
          'description' => 'Yearly receipt yes/no',
          'html' => array(
            'type' => 'Text',
          ) ,
        ) ,
        'issued' => array(
          'name' => 'issued',
          'type' => CRM_Utils_Type::T_DATE + CRM_Utils_Type::T_TIME,
          'title' => ts('Issued Date') ,
          'description' => 'when receipt was issued',
          'headerPattern' => '/receive(.?date)?/i',
          'dataPattern' => '/^\d{4}-?\d{2}-?\d{2} ?(\d{2}:?\d{2}:?(\d{2})?)?$/',
          'html' => array(
            'type' => 'Select Date',
          ) ,
        ) ,

      );
    }
    return self::$_fields;
  }
  /**
   * Returns an array containing, for each field, the arary key used for that
   * field in self::$_fields.
   *
   * @return array
   */
  static function &fieldKeys()
  {
    if (!(self::$_fieldKeys)) {
      self::$_fieldKeys = array(
        'id' => 'id',
        'contribution_id' => 'contribution_id',
        'number' => 'number',
      );
    }
    return self::$_fieldKeys;
  }
  /**
   * Returns the names of this table
   *
   * @return string
   */
  static function getTableName()
  {
    return CRM_Core_DAO::getLocaleTableName(self::$_tableName);
  }
  /**
   * Returns if this table needs to be logged
   *
   * @return boolean
   */
  function getLog()
  {
    return self::$_log;
  }
  /**
   * Returns the list of fields that can be imported
   *
   * @param bool $prefix
   *
   * @return array
   */
  static function &import($prefix = false)
  {
    if (!(self::$_import)) {
      self::$_import = array();
      $fields = self::fields();
      foreach($fields as $name => $field) {
        if (CRM_Utils_Array::value('import', $field)) {
          if ($prefix) {
            self::$_import['receiptnumber'] = & $fields[$name];
          } else {
            self::$_import[$name] = & $fields[$name];
          }
        }
      }
    }
    return self::$_import;
  }
  /**
   * Returns the list of fields that can be exported
   *
   * @param bool $prefix
   *
   * @return array
   */
  static function &export($prefix = false)
  {
    if (!(self::$_export)) {
      self::$_export = array();
      $fields = self::fields();
      foreach($fields as $name => $field) {
        if (CRM_Utils_Array::value('export', $field)) {
          if ($prefix) {
            self::$_export['receiptnumber'] = & $fields[$name];
          } else {
            self::$_export[$name] = & $fields[$name];
          }
        }
      }
    }
    return self::$_export;
  }
}
