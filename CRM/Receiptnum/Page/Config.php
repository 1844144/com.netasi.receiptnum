<?php

require_once 'CRM/Core/Page.php';

class CRM_Receiptnum_Page_Config extends CRM_Core_Page {
  function run() {

    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(ts('Tax Receipt Printing Config'));

    parent::run();
  }
}
