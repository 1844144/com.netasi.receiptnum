<?php

class CRM_Receiptnum_BAO_ReceiptNumber extends CRM_Receiptnum_DAO_ReceiptNumber {

  /**
   * Create a new ReceiptNumber based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Receiptnum_DAO_ReceiptNumber|NULL
   *
  public static function create($params) {
    $className = 'CRM_Receiptnum_DAO_ReceiptNumber';
    $entityName = 'ReceiptNumber';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } 
  */


}
