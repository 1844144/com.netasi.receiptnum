<?php

//  FOR ALL EXAMPLESS SEE HRJOB CIVICRM EXTENSTION ON GIT HUB

class CRM_Receiptnum_BAO_Query extends CRM_Contact_BAO_Query_Interface {
  /**
   * static field for all the export/import hrjob fields
   *
   * @var array
   * @static
   */
  static $_hrjobFields = array();
  /**
   * Function get the import/export fields for hrjob
   *
   * @return array self::$_hrjobFields  associative array of hrjob fields
   * @static
   */
  function &getFields() {
    if (!self::$_hrjobFields) {
      self::$_hrjobFields['rcpt_num'] =
        array(
          'name'  => 'rcpt_num',
          'title' => 'Tax Receipt Number',
          'type'  => CRM_Utils_Type::T_INT,
          'where' => 'civicrm_receiptnumber.number'
        );
    }
    return self::$_hrjobFields;
  }
  function select(&$query) {
  }
  function where(&$query) {
    $grouping = NULL;
    foreach (array_keys($query->_params) as $id) {
      if (empty($query->_params[$id][0])) {
        continue;
      }

      if (substr($query->_params[$id][0], 0, 5) == 'rcpt_') {
        $this->whereClauseSingle($query->_params[$id], $query);
      }
    }
  }
  function whereClauseSingle(&$values, &$query) {
    list($name, $op, $value, $grouping, $wildcard) = $values;
    $fields = $this->getFields();

    $display = $options = $value;
    $query->_qill[$grouping][]  = ts('%1 %2', array(1 => $fields[$name]['title'], 2 => $op)) . ' ' . $display;
    // evar_dump(CRM_Contact_BAO_Query::buildClause($fields[$name]['where'], $op, $options));
    $query->_where[$grouping][] = CRM_Contact_BAO_Query::buildClause($fields[$name]['where'], $op, $options);
    list($tableName, $fieldName) = explode('.', $fields[$name]['where'], 2);
    $query->_tables[$tableName]  = $query->_whereTables[$tableName] = 1;
        
    return;
  }
  function from($name, $mode, $side) {
    if ($name == "civicrm_contribution") {

      $from = " $side JOIN civicrm_receiptnumber ON civicrm_contribution.id = civicrm_receiptnumber.contribution_id ";
      return $from;
    }
  }
  public function getPanesMapper(&$panes) {
    return;
  }

}