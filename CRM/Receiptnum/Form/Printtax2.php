<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Receiptnum_Form_Printtax2 extends CRM_Core_Form {

  function calulate_total($con_ids){
    $con_ids = join(',',$con_ids);

    // calculates total of ids
    $sql = " select sum(total_amount) as total from civicrm_contribution where id in ({$con_ids})";
    $dao = CRM_Core_DAO::executeQuery($sql);
    if ($dao->fetch()){
      return $dao->total;
    }
    else return 0;
  }

  function get_spouse($cid) {
    // Spouse if no Contribution 
    $spouse = "";
    $year = date("Y",strtotime("-1 year"));
    // check if spouse has contribution pas year
    $sql = "SELECT con.display_name, contrib.id from civicrm_relationship rel  
    LEFT join civicrm_contact con on rel.contact_id_a=con.id
    left outer join (
      select ctr.id, ctr.contact_id from civicrm_contribution ctr
      left join civicrm_financial_type ft on ft.id=ctr.financial_type_id
      where  ft.is_deductible=1 and year(ctr.receive_date)=$year 
    ) contrib on contrib.contact_id=con.id                 
    where rel.relationship_type_id=2 and rel.contact_id_b={$cid} and contrib.id is NULL ";
    $dao = CRM_Core_DAO::executeQuery($sql);
    if ($dao->fetch()){
      $spouse = "and {$dao->display_name}";
    }
    else {
      // back relationship
      $sql = "SELECT con.display_name, contrib.id from civicrm_relationship rel  
            LEFT join civicrm_contact con on rel.contact_id_b=con.id
            left outer join (
              select ctr.id, ctr.contact_id from civicrm_contribution ctr
              left join civicrm_financial_type ft on ft.id=ctr.financial_type_id
              where  ft.is_deductible=1 and year(ctr.receive_date)=$year 
            ) contrib on contrib.contact_id=con.id                 
            where rel.relationship_type_id=2 and rel.contact_id_a={$cid} and contrib.id is NULL ";
      $dao = CRM_Core_DAO::executeQuery($sql);
      if ($dao->fetch())  {
        $spouse = "and {$dao->display_name}";
      }
    }
    return $spouse;
  }

  function generate_html($contribution, $contact, $templateId, $yearly_ids = Null){
        // Generate message for $contribution by $contact using $templateId

        CRM_Contribute_Form_Task_Receiptnum::check_config();


        $result = civicrm_api3('MessageTemplate', 'get', array(
          'sequential' => 1,
          'id' => $templateId,
        ));

        $html_message = $result['values'][0]['msg_html'];
        CRM_Contact_Form_Task_PDFLetterCommon::formatMessage($html_message);

        $messageToken = CRM_Utils_Token::getTokens($html_message);
        $domain = CRM_Core_BAO_Domain::getDomain();

        $tokens = array();
        CRM_Utils_Hook::tokens($tokens);
        $tokens = array_merge($tokens, $messageToken);

        $categories = array_keys($tokens);

        $returnProperties = array();
        if (isset($tokens['contact'])) {
            foreach ($tokens['contact'] as $key => $value) {
              if (!isset($returnProperties[$value])) {
                $returnProperties[$value] = 1;
              }
            }
        }

        $contact_id = $contact;
        $params = array('contact_id' => $contact);
        list($contacts) = CRM_Utils_Token::getTokenDetails(
            $params,
            $returnProperties,
            False,
            False,
            array(),
            $messageToken
        );
        $contact = $contacts[$contact];
        // evar_dump('generate_html');
        // evar_dump($messageToken);
        // evar_dump($contacts);

        $returnProperties = array();
        if (isset($tokens['contribution'])) {
            foreach ($tokens['contribution'] as $key => $value) {
              if (!isset($returnProperties[$value])) {
                $returnProperties[$value] = 1;
              }
            }
        }

        // $contribution_details = CRM_Utils_Token::getContributionTokenDetails(array('contribution_id' => $contribution),
        // $returnProperties,
        // NULL,
        // $messageToken
        // );
        // $contribution = $contribution_details[$contribution];
        $contribution = civicrm_api3('Contribution', 'get', array('id' => $contribution));
        $tokenHtml = CRM_Utils_Token::replaceContactTokens($html_message, $contact, TRUE, $messageToken);

        // replace receipt Number token
        $res = civicrm_api3('ReceiptNumber', 'get', array(
        'sequential' => 1,
        'contribution_id' => $contribution['id'],
        'options' => array('sort' => "number desc"),
        ));
        if ($res['count'] != 0 ){
            $receiptnum = $res['values'][0]['number'];
            // change receipt number 
            if($res['values'][0]['oldnumber'] != 0){
              $tokenHtml = "<div class='dup-container'><div class='oldnumber'>* CANCELS AND REPLACES RECEIPT #{$res['values'][0]['oldnumber']} *</div></div>". $tokenHtml;
            }
            $tokenHtml = str_replace("{tax_receipt.number}", $receiptnum, $tokenHtml);
            $issued = date('F j, Y', strtotime(date($res['values'][0]['issued'])));
            $tokenHtml = str_replace("{tax_receipt.issued}", $issued, $tokenHtml);
            $tokenHtml = str_replace("{tax_receipt.spouse_if_no_contribution}", self::get_spouse($contact_id), $tokenHtml);
            if ($yearly_ids != Null){
              $tokenHtml = str_replace("{tax_receipt.yearly_total}", self::calulate_total($yearly_ids), $tokenHtml);
            }

            if($res['values'][0]['printed'] != 0){
              $tokenHtml = "<div class='dup-container' ><div class='duplicate'>DUPLICATE</div></div>". $tokenHtml;
            }
        } 

        $tokenHtml = CRM_Utils_Token::replaceDomainTokens($tokenHtml, $domain, TRUE, $categories);
        $tokenHtml = CRM_Utils_Token::replaceContributionTokens($tokenHtml, $contribution, TRUE, $messageToken);
        $tokenHtml = CRM_Utils_Token::replaceComponentTokens($tokenHtml, $contact, $categories);
        $tokenHtml = CRM_Utils_Token::replaceHookTokens($tokenHtml, $contact, $categories, TRUE);
        $tokenHtml = CRM_Utils_Token::parseThroughSmarty($tokenHtml, $contact);

        self::mark_as_printed($contribution['id']);
        if ($yearly_ids != Null) {
          foreach ($yearly_ids as $con_id) {
            self::mark_as_printed($con_id);
          }
        }

        $tokenHtml = '<div class="print-page">' . $tokenHtml . "</div>";

        // now when page is ready 
        // if it is Quebec we need to double it
        $sql = "SELECT * from civicrm_contact con 
          left join civicrm_address adr on adr.contact_id=con.id
          where adr.is_primary=1 and adr.state_province_id = 1110 and con.id={$contact_id}
        ";
        $dao = CRM_Core_DAO::executeQuery($sql);
        if ($dao->fetch()) {
          $tokenHtml .= $tokenHtml;
        }
        return $tokenHtml;
    }

    function mark_as_printed($id){
        $sql = " update civicrm_receiptnumber rn set printed=1 
          where contribution_id = {$id}";
        $dao = CRM_Core_DAO::executeQuery($sql);
    }



  function buildQuickForm() {

    $session = CRM_Core_Session::singleton();
    $contactIds = $session->get('contactIds');
    $contributionIds = $session->get('contributionIds');
    $yearly_receipts = $session->get('yearly_receipts');

    $to_skip_list = $session->get('to_skip_list');
    $to_assign_list = $session->get('to_assign_list');

    // evar_dump($to_skip_list);

    $assign_ids = array();
    foreach ($to_assign_list as $record) $assign_ids[] = $record['id'];
    $skip_ids = array();
    foreach ($to_skip_list as $record) $skip_ids[] = $record['id'];
    
    CRM_Contribute_Form_Task_Receiptnum::assign_receipt_number($assign_ids);

    $out = array();
    $tmpl = get_option('receiptnum_template_usual', NULL);
    foreach ($contributionIds as $key => $value) {
        if (! in_array($value, $skip_ids)){
            $out[] = self::generate_html($value, $contactIds[$key], $tmpl);
        }
    }
    
    // and now we will add yearly duplicate/lost printing
    $tmpl = get_option('receiptnum_template_yearly', NULL);
    foreach ($yearly_receipts as $rec) {
      $receipt_ids = CRM_Contribute_Form_Task_Printtax::get_receipt_ids($rec);
      $contrib = $receipt_ids[count($receipt_ids)-1];
      
      $sql = "select contact_id from civicrm_contribution where id={$contrib}";
      $dao = CRM_Core_DAO::executeQuery($sql);
      $dao->fetch();
      $contact = $dao->contact_id;
      
      $out[] = self::generate_html($contrib, $contact, $tmpl, $receipt_ids);
    }

    if (empty($out)) $out[] = "All receipts for all tax deductible contributions have already been printed.";


    // $this->controller->setEmbedded(1);
    $this->assign('html_array', $out);
    $session->set('contactIds',array());
    $session->set('contributionIds',array());
    $session->set('yearly_receipts',array());


    $out =  join('',$out);

    $out = '<html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <style>@page { margin: 0.4in 0.3in 0.1in 0.3in; }</style>
          <style type="text/css" src="/wp-content/plugins/civicrm/civicrm/css/print.css">);</style>
    
        </head>
      <body>
        <div id="crm-container"> '  
        . file_get_contents(dirname(__FILE__).'/../../Contribute/Form/Task/Style.tpl') 
        . $out
        . ' </body></html>';
    echo $out;
    CRM_Utils_System::civiExit(1);
    $this->controller->setPrint(1);
    CRM_Utils_System::setTitle('Tax Receipt');


    // export form elements
    parent::buildQuickForm();
  }

  // function postProcess() {
  //   $values = $this->exportValues();
  //   $options = $this->getColorOptions();
  //   CRM_Core_Session::setStatus(ts('You picked color "%1"', array(
  //     1 => $options[$values['favorite_color']]
  //   )));
  //   parent::postProcess();
  // }

  

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }
}
