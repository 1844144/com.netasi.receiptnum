<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Receiptnum_Form_Config extends CRM_Core_Form {
  public function buildQuickForm() {

    CRM_Utils_System::setTitle(ts('Tax Receipt Printing Config'));

    $result = civicrm_api3('MessageTemplate', 'get', array(
      'sequential' => 1,
      'workflow_id' => array('IS NULL' => 1),
    ));

    $templates = array();
    foreach ($result['values'] as $tpl) {
      $templates[ intval($tpl['id'])] = ts($tpl['msg_title']); 
    }



    $selector = $this->add('select', 'usual_template', ts('Template to print individual receipts: '),
      array(0 => ts('- not selected -')) + $templates
    );
    $selector->setSelected( get_option ('receiptnum_template_usual', 0)  );
    
    $selector = $this->add('select', 'yearly_template', ts('Template to print year-end receipts: '),
      array(0 => ts('- not selected -')) + $templates
    );
    $selector->setSelected( get_option ('receiptnum_template_yearly', 0)  );

    $this->addButtons(array(
        array(
          'type' => 'next',
          'name' => ts('Save'),
          'isDefault' => TRUE,
        ),
        // array(
        //   'type' => 'back',
        //   'name' => ts('Cancel'),
        // ),
      )
    );
  }

  public function postProcess() {
    
    extract($this->_submitValues);
    update_option ('receiptnum_template_usual', $usual_template);
    update_option ('receiptnum_template_yearly', $yearly_template);

    $session = CRM_Core_Session::singleton();
    $session->set('my_message_template',$this->_submitValues['message_template']);


    // $this->controller->resetPage('CRM_Html4contribution_Form_GenHtml');
    $session->setStatus(ts('Tax Receipt Options'),ts('Saved'),  'success');
    $session->replaceUserContext(CRM_Utils_System::url('civicrm/receiptnum/config'));
    
    // CRM_Utils_System::redirect(CRM_Utils_System::url("civicrm/genhtml"));
  }

  function getColorOptions() {
    $options = array(
      '' => ts('- select -'),
      '#f00' => ts('Red'),
      '#0f0' => ts('Green'),
      '#00f' => ts('Blue'),
      '#f0f' => ts('Purple'),
    );
    foreach (array('1','2','3','4','5','6','7','8','9','a','b','c','d','e') as $f) {
      $options["#{$f}{$f}{$f}"] = ts('Grey (%1)', array(1 => $f));
    }
    return $options;
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }
}
