{* HEADER *}


{literal}
<style>
	@page { margin: 0.4in 0.3in 0.1in 0.3in; }
	.dup-container {
		position: relative;
	}
	.duplicate {
		position: absolute;
    top: 150px;
    border: 4px solid red;
    color: red;
    display: inline-block;
    left: 50%;
    transform: rotate(-10deg);
    font-size: 50px;
    padding: 13px;
	}
	.oldnumber {
               position: absolute;
           top: 70px;
           color: black;
           font-weight: bold;
           display: inline-block;
           left: 30px;
       }
	
@media print {
	.print-page{
		page-break-after: always;
		page-break-inside: avoid;
	}

}
</style>
{/literal}
{foreach from=$html_array item=it}
	{$it}
	<div class='print-page'></div>
{/foreach}

