
Number of tax deductible contributions: {$items_count}
{if $already_printed}
<h2>
Tax receipts for these contributions have already been printed. 
</h2>
	<table>
		<tr> <th> ID <th> Name <th> Amount <th> Receipt Number
		{foreach from=$already_printed item=it}
			<tr> 
			<td> {$it.id}
			<td> {$it.name}
			<td> {$it.amount}
			<td> {$it.number}
		{/foreach}
	</table>

	<h4> {$form.printed.label} {$form.printed.html} <h4>
{/if}

{if $not_assigned_list}
<h2>	
 Tax receipt numbers may be issued for these contributions.
</h2> 

	<table>
		<tr> <th> ID <th> Name <th> Amount
		{foreach from=$not_assigned_list item=it}
			<tr> 
			<td> {$it.id}
			<td> {$it.name}
			<td> {$it.amount}
		{/foreach}
	</table>
	<h4> {$form.not_assigned.label} {$form.not_assigned.html} </h4>
{/if}


{* FOOTER *}
<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>
