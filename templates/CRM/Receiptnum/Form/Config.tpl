<!-- <div id="help">
    {ts}Here you may choose respective templates (from Message Templates) for using in Tax Receipts Printing{/ts}
</div>
 -->
<table class="form-layout-compressed">
  <tr id="selectPdfFormat" >
    <td>{$form.usual_template.label} {$form.usual_template.html}</td>
  </tr>
  <tr id="selectPdfFormat" >
    <td>{$form.yearly_template.label} {$form.yearly_template.html}</td>
  </tr>

</table>

<div class="spacer"></div>
<div class="form-item">
 {$form.buttons.html}
</div>
